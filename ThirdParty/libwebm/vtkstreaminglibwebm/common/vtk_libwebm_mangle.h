#ifndef vtklibwebm_mangle_h
#define vtklibwebm_mangle_h

// Mangle all global namespaces
#define libwebm vtklibwebm_libwebm
#define mkvmuxer vtklibwebm_mkvmuxer
#define mkvparser vtklibwebm_mkvparser

#endif // vtklibwebm_mangle_h
